import 'dart:async';
import 'dart:math';
import 'dart:ui';

import 'package:bme/models/model_feed.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:bme/bme/bme.dart';
import 'package:bme/main.dart';
import 'package:bme/widgets/DismissibleWrapper.dart';
import 'package:bme/widgets/circular_clip.dart';
import 'package:bme/widgets/frosted_glas.dart';
import 'package:bme/widgets/loading_list_view.dart';
import 'package:bme/widgets/post.dart';
import 'package:bme/widgets/post_create.dart';

class Feed extends StatefulWidget {

  final int pageSize;
  final int pageThreshold;

  Feed({
    Key key,
    this.pageSize: 50,
    this.pageThreshold: 10
  }) :super(key: key);


  @override
  State<StatefulWidget> createState() {
    return new _FeedState();
  }


}

class _FeedState extends State<Feed> {

  BuildContext _buildContext;


  @override
  Widget build(BuildContext context) {
    if (!bme.socket_feed.hasListener) {
      bme.result_feed.listen((data) {
        _insert(data);
//        _list.
      });
    }
    return new MaterialApp(
      home: new Scaffold(
        appBar: new AppBar(
          title: const Text('AnimatedList'),
          actions: <Widget>[
            new IconButton(
              icon: const Icon(Icons.add_circle),
//              onPressed: _insert,
              tooltip: 'insert a new item',
            ),
            new IconButton(
              icon: const Icon(Icons.remove_circle),
              onPressed: _remove,
              tooltip: 'remove the selected item',
            ),
          ],
        ),
        body: new Padding(
          padding: const EdgeInsets.all(16.0),
          child: new AnimatedList(
            key: _listKey,
            initialItemCount: 0,
            itemBuilder: _buildItem,
          ),
        ),
      ),
    );
  }
//
//  void onPosted(Map post){
//    _postStreamController.add(post);
//  }

  // Used to build list items that haven't been removed.
  Widget _buildItem(
      BuildContext context, int index, Animation<double> animation) {
    return new CardItem(
      animation: animation,
      item: _list[index],
      selected: _selectedItem == _list[index],
      onTap: () {
        setState(() {
          _selectedItem = _selectedItem == _list[index] ? null : _list[index];
        });
      },
    );
  }

  // Used to build an item after it has been removed from the list. This method is
  // needed because a removed item remains  visible until its animation has
  // completed (even though it's gone as far this ListModel is concerned).
  // The widget will be used by the [AnimatedListState.removeItem] method's
  // [AnimatedListRemovedItemBuilder] parameter.
  Widget _buildRemovedItem(
      ModelFeed item, BuildContext context, Animation<double> animation) {
    return new CardItem(
      animation: animation,
      item: item,
      selected: false,
      // No gesture detector here: we don't want removed items to be interactive.
    );
  }

  // Insert the "next item" into the list model.
  void _insert(data) {
    final int index = _selectedItem == null ? _list.length : _list.indexOf(_selectedItem);
//    if (_list.length>0)
      _list.insert(index, data);
  }

  // Remove the selected item from the list model.
  void _remove() {
    if (_selectedItem != null) {
      _list.removeAt(_list.indexOf(_selectedItem));
      setState(() {
        _selectedItem = null;
      });
    }
  }


  final GlobalKey<AnimatedListState> _listKey = new GlobalKey<AnimatedListState>();
  ListModel<ModelFeed> _list;
  ModelFeed _selectedItem;
  int _nextItem; // The next item inserted when the user presses the '+' button.

  @override
  void initState() {
    super.initState();
    _list = new ListModel<ModelFeed>(
      listKey: _listKey,
      removedItemBuilder: _buildRemovedItem,
    );
    _nextItem = 3;
  }

//  Future<List<Map>> request(int page, int pageSize) async {
//    return bme.api.getFeed(paginationIndex: page, paginationSize: pageSize);
//  }

  Widget adapt(Map map) {
    return new Post(map);
  }


}

class FeedActionButton extends StatefulWidget {

  final PostedCallback onPosted;

  FeedActionButton({this.onPosted});

  @override
  State<StatefulWidget> createState() {
    return new _FeedActionButtonState();
  }
}

class _FeedActionButtonState extends State<FeedActionButton> {

  BuildContext _context;

  @override
  Widget build(BuildContext context) {
    _context = context;
    return new FloatingActionButton(
        child: new Icon(
            Icons.add,
            color: Theme
                .of(context)
                .iconTheme
                .color
        ),
//        onPressed: onPressed,
        backgroundColor: Colors.white
    );
  }

//  onPressed() async {
//    Map post = await Navigator.of(_context).push(new _PostCreateRoute(_context));
//    if(post != null && widget.onPosted!=null){
//      this.widget.onPosted(post);
//    }
//  }
}
//
//class _PostCreateRoute extends TransitionRoute with LocalHistoryRoute {
//
//  final BuildContext context;
//
//  bool dismissed = false;
//
//  _PostCreateRoute(this.context);
//
//
//  @override
//  Iterable<OverlayEntry> createOverlayEntries() {
//    var overlay = new OverlayEntry(builder: (context) {
//      Widget w;
//      w = new AnimatedBuilder(animation: animation, builder: (context, widget) {
//        Widget w = new PostCreate(onDismiss: onDismissed, onPosted: onPosted,);
//        print("animation ${animation.value}");
//        w = new Material(child: w, color: Colors.white.withOpacity(0.5),);
//        w = new CircularClip(child: w, clip: animation.value,);
//        w =
//        new FrostedGlass(child: w, sigma: animation.value * 5.0, opacity: 0.0,);
//
//        w = new DismissibleWrapper(child: w, direction: DismissDirection.down,
//          onDismissed: (x) => onDismissed(),
//          key: new Key("POST_CREATE"),);
//
//
//        return w;
//      });
//      return w;
//    },
//        opaque: false,
//        maintainState: true);
//
//    return [overlay];
//  }
//
//  void onPosted(Map post){
//    Navigator.of(context).pop(post);
//  }
//
//  void onDismissed() {
//    this.controller.value=0.0;
//    Navigator.of(context).pop();
//  }
//
//  // TODO: implement opaque
//  @override
//  bool get opaque => false;
//
//  // TODO: implement transitionDuration
//  @override
//  Duration get transitionDuration => const Duration(milliseconds: 400);
//}
//
class ListModel<ModelFeed> {
  ListModel({
    @required this.listKey,
    @required this.removedItemBuilder

  }):_items = new List<ModelFeed>();

  final GlobalKey<AnimatedListState> listKey;
  final dynamic removedItemBuilder;
  final List<ModelFeed> _items;

  AnimatedListState get _animatedList => listKey.currentState;

  void insert(int index, ModelFeed item) {
    _items.insert(index, item);
    _animatedList.insertItem(index);
  }

  ModelFeed removeAt(int index) {
    final ModelFeed removedItem = _items.removeAt(index);
    if (removedItem != null) {
      _animatedList.removeItem(index,
              (BuildContext context, Animation<double> animation) {
            return removedItemBuilder(removedItem, context, animation);
          });
    }
    return removedItem;
  }

  int get length => _items!=null?_items.length:0;

  ModelFeed operator [](int index) => _items[index];

  int indexOf(ModelFeed item) => _items.indexOf(item);
}

/// Displays its integer item as 'item N' on a Card whose color is based on
/// the item's value. The text is displayed in bright green if selected is true.
/// This widget's height is based on the animation parameter, it varies
/// from 0 to 128 as the animation varies from 0.0 to 1.0.
class CardItem extends StatelessWidget {
  const CardItem(
      {Key key,
        @required this.animation,
        this.onTap,
        @required this.item,
        this.selected: false})
      : assert(animation != null),
        assert(selected != null),
        super(key: key);

  final Animation<double> animation;
  final VoidCallback onTap;
  final ModelFeed item;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.display1;
    if (selected)
      textStyle = textStyle.copyWith(color: Colors.lightGreenAccent[400]);
    return new Padding(
      padding: const EdgeInsets.all(2.0),
      child: new SizeTransition(
        axis: Axis.vertical,
        sizeFactor: animation,
        child: new GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: onTap,
          child: new SizedBox(
            height: 128.0,
            child: new Card(
              color: Colors.primaries[0 % Colors.primaries.length],
              child: new Center(
                child: new Text('Item '+item.title, style: textStyle),
              ),
            ),
          ),
        ),
      ),
    );
  }
}