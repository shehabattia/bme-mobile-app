class ModelFeed {
  int id;
  int group_id;
  String type;
  int user_id;
  String title;
  String body;
  int media_id;
  var timestamp;
  int event_date;
  bool is_send_notification;
  bool is_hidden;
  //TODO: setters and getters
  ModelFeed(
  this.id,
      this.group_id,
      this.type,
      this.user_id,
      this.title,
      this.body,
      this.media_id,
      this.timestamp,
      this.event_date,
      this.is_send_notification,
      this.is_hidden
      ) ;

  ModelFeed.fromJson(Map<dynamic, dynamic> json)
      : id = json['id'],
        group_id = json['group_id'],
        type = json['type'],
        user_id =  json['user_id'],
        title = json['title'],
        body = json['body'],
        media_id =  json['media_id'],
        timestamp =  json['timestamp'],
        event_date = json['event_date'],
        is_send_notification = json['is_send_notification'],
        is_hidden = json['is_hidden'];

  Map<dynamic, dynamic> toJson() => {
        'id': id,
        'group_id': group_id,
        'type': type,
        'user_id': user_id,
        'title': title,
        'body': body,
        'media_id': media_id,
        'timestamp': timestamp,
        'event_date': event_date,
    'is_send_notification': is_send_notification,
    'is_hidden': is_hidden,
      };
}
