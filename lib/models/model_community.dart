class ModelCommunity {
  int community_server_id;
  int community_local_id; //For local caching compatibility.
  String name;
  String about;
  String website;
  bool is_invite_only;
  int timestamp;
  int user_id;
  int role_to_create_groups;
  int role_to_archive_groups;
  int role_to_archive_community;
  bool is_archived;
  //TODO: setters and getters
  ModelCommunity(
      this.community_server_id,
      this.community_local_id, //For local caching compatibility.
      this.name,
      this.about,
      this.website,
      this.is_invite_only,
      this.timestamp,
      this.user_id,
      this.role_to_create_groups,
      this.role_to_archive_groups,
      this.role_to_archive_community,
      this.is_archived
      ) {
  }

  ModelCommunity.fromJson(Map<String, dynamic> json)
      : community_server_id = json['community_server_id'],
        community_local_id = json['community_local_id'],
        name = json['name'],
        about = json['about'],
        website = json['website'],
        is_invite_only = json['is_invite_only'],
        timestamp = json['timestamp'],
        user_id = json['user_id'],
        role_to_create_groups = json['role_to_create_groups'],
        role_to_archive_groups = json['role_to_archive_groups'],
        role_to_archive_community = json['role_to_archive_community'],
        is_archived = json['is_archived'];

  Map<String, dynamic> toJson() => {
    'community_server_id': community_server_id,
    'community_local_id': community_local_id,
    'name': name,
    'about': about,
    'website': website,
    'is_invite_only': is_invite_only,
    'timestamp': timestamp,
    'user_id': user_id,
    'role_to_create_groups': role_to_create_groups,
    'role_to_archive_groups': role_to_archive_groups,
    'role_to_archive_community': role_to_archive_community,
    'is_archived': is_archived,
  };
}
