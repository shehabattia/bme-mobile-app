class ModelGroup {
  int group_server_id;
  int group_local_id; //For local caching compatibility.
  int community_id;
  String name;
  bool is_private;
  double dues_amount;
  bool force_show_in_feed;
  bool default_show_in_feed;
  int role_to_post;
  int role_to_reply;
  bool is_archived;
  //TODO: setters and getters
  ModelGroup(
      this.group_server_id,
      this.group_local_id, //For local caching compatibility.
      this.community_id,
      this.name,
      this.is_private,
      this.dues_amount,
      this.force_show_in_feed,
      this.default_show_in_feed,
      this.role_to_post,
      this.role_to_reply,
      this.is_archived,
      ) {
  }

  ModelGroup.fromJson(Map<String, dynamic> json)
      : group_server_id = json['group_server_id'],
        group_local_id = json['group_local_id'],
        community_id = json['community_id'],
        name = json['name'],
        is_private = json['is_private'],
        dues_amount = json['dues_amount'],
        force_show_in_feed = json['force_show_in_feed'],
        default_show_in_feed = json['default_show_in_feed'],
        role_to_post = json['role_to_post'],
        role_to_reply = json['role_to_reply'],
        is_archived = json['is_archived'];

  Map<String, dynamic> toJson() => {
    'group_server_id': group_server_id,
    'group_local_id': group_local_id,
    'community_id': community_id,
    'name': name,
    'is_private': is_private,
    'dues_amount': dues_amount,
    'force_show_in_feed': force_show_in_feed,
    'default_show_in_feed': default_show_in_feed,
    'role_to_post': role_to_post,
    'role_to_reply': role_to_reply,
    'is_archived': is_archived,
  };
}
