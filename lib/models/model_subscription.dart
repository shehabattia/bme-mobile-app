class ModelSubscription {
  int subscription_server_id;
  int subscription_local_id; //For local caching compatibility.
  int group_id;
  int user_id;
  int role;
  bool is_paid_dues;
  bool show_in_feed;
  bool show_notifications;
  int date_joined;
  bool is_active;
  //TODO: setters and getters
  ModelSubscription(
      this.subscription_server_id,
      this.subscription_local_id, //For local caching compatibility.
      this.group_id,
      this.user_id,
      this.role,
      this.is_paid_dues,
      this.show_in_feed,
      this.show_notifications,
      this.date_joined,
      this.is_active,
      ) {
  }

  ModelSubscription.fromJson(Map<String, dynamic> json)
      : subscription_server_id = json['subscription_server_id'],
        subscription_local_id = json['subscription_local_id'],
        group_id = json['group_id'],
        user_id = json['user_id'],
        role = json['role'],
        is_paid_dues = json['is_paid_dues'],
        show_in_feed = json['show_in_feed'],
        show_notifications = json['show_notifications'],
        date_joined = json['date_joined'],
        is_active = json['is_active'];

  Map<String, dynamic> toJson() => {
    'subscription_server_id': subscription_server_id,
    'subscription_local_id': subscription_local_id,
    'community_id': group_id,
    'name': user_id,
    'is_private': role,
    'dues_amount': is_paid_dues,
    'force_show_in_feed': show_in_feed,
    'default_show_in_feed': show_notifications,
    'role_to_post': date_joined,
    'role_to_reply': is_active,
  };
}
