class ModelReaction {
  int reaction_server_id;
  int reaction_local_id; //For local caching compatibility.
  int feed_id;
  int reply_id;
  int user_id;
  String type;
  int timestamp;
  //TODO: setters and getters
  ModelReaction(
      this.reaction_server_id,
      this.reaction_local_id, //For local caching compatibility.
      this.feed_id,
      this.reply_id,
      this.user_id,
      this.type,
      this.timestamp,
      ) {
  }
  ModelReaction.fromJson(Map<String, dynamic> json)
      : reaction_server_id = json['reaction_server_id'],
        reaction_local_id = json['reaction_local_id'],
        feed_id = json['feed_id'],
        reply_id = json['reply_id'],
        user_id = json['user_id'],
        type = json['type'],
        timestamp = json['timestamp'];

  Map<String, dynamic> toJson() => {
    'reaction_server_id': reaction_server_id,
    'reaction_local_id': reaction_local_id,
    'feed_id': feed_id,
    'reply_id': reply_id,
    'user_id': user_id,
    'type': type,
    'timestamp': timestamp,
  };
}
