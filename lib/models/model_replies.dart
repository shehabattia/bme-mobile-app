class ModelReplies {
  int replies_server_id;
  int replies_local_id; //For local caching compatibility.
  int feed_id;
  String message;
  int user_id;
  int timestamp;
  bool is_hidden;
  //TODO: setters and getters
  ModelReplies(
      this.replies_server_id,
      this.replies_local_id, //For local caching compatibility.
      this.feed_id,
      this.message,
      this.user_id,
      this.timestamp,
      this.is_hidden,
      ) {
  }

  ModelReplies.fromJson(Map<String, dynamic> json)
      : replies_server_id = json['replies_server_id'],
        replies_local_id = json['replies_local_id'],
        feed_id = json['feed_id'],
        message = json['message'],
        user_id = json['user_id'],
        timestamp = json['timestamp'],
        is_hidden = json['is_hidden'];

  Map<String, dynamic> toJson() => {
    'replies_server_id': replies_server_id,
    'replies_local_id': replies_local_id,
    'feed_id': feed_id,
    'message': message,
    'user_id': user_id,
    'timestamp': timestamp,
    'is_hidden': is_hidden,
  };
}
