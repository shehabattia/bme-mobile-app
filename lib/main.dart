import 'package:bme/models/model_feed.dart';
import 'package:flutter/material.dart';
import 'package:bme/bme/bme.dart';
import 'package:bme/widgets/community.dart';
import 'package:bme/widgets/feed.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:connectivity/connectivity.dart';

import 'dart:convert';

void main() {
  runApp(new BmeApp());
}

var connectedToInternetSubscription;



/// Returns the color scheme used by bme
MaterialColor bmeColor() {
  return new MaterialColor(0xFF0498C1, {
    50: new Color(0xFFE1F3F8),
    100: new Color(0xFFB4E0EC),
    200: new Color(0xFF82CCE0),
    300: new Color(0xFF4FB7D4),
    400: new Color(0xFF2AA7CA),
    500: new Color(0xFF0498C1),
    600: new Color(0xFF0390BB),
    700: new Color(0xFF0385B3),
    800: new Color(0xFF027BAB),
    900: new Color(0xFF016A9E)
  });
}

class BmeApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Bme bme = new Bme();
    return new MaterialApp(
      title: 'Bme',
      theme: new ThemeData(
          primarySwatch: bmeColor(),
          scaffoldBackgroundColor: Colors.white,
          primaryColor: bmeColor(), backgroundColor: Colors.white),
      home: new MainPage(bme),
      showPerformanceOverlay: false,
    );
  }
}

class MainPage extends StatefulWidget {
  final Bme bme;

  MainPage(this.bme);

  @override
  State<StatefulWidget> createState() {
    return new _MainPageState();
  }
}

class _MainPageState extends State<MainPage> {

  PageController pageController;
  int page = 0;


  @override
  Widget build(BuildContext context) {

    return new Scaffold(
        backgroundColor: Colors.white,
        body: new PageView(
            children: [
              new Feed(key: new Key("FEED")),
              new Community()
            ],
            controller: pageController,
            onPageChanged: onPageChanged
        ),
        bottomNavigationBar: new BottomNavigationBar(
            items: [
              new BottomNavigationBarItem(
                icon: new Icon(new IconData(62008, fontFamily: "mdi")),
                title: new Text("feed"),
              ),
              new BottomNavigationBarItem(
                  icon: new Icon(Icons.people), title: new Text("community")),
            ],
            onTap: onTap,
            currentIndex: page
        )
    );
  }
  IOWebSocketChannel channel;
  void startSocket() async{

    print("Starting socket connection.");
    try{

      channel = await IOWebSocketChannel.connect('ws://bmeserver.ddns.net/');
      channel.sink.add("{\"userid\":1}");
      print("Should be connected");
      var socket = channel.stream.listen((data){
      });
      socket.onError((error){
        print("socket error $error");
      });
      socket.onData((data){
        data = json.decode(data);
        if(data['request']=='feed') {
          print("received "+data['data'].toString());
          bme.query_feed.add(ModelFeed.fromJson(data['data']));
        }
      });
    }
    catch(e){
      print("error $e");
    }

  }


  @override
  void initState() {
    super.initState();
    pageController = new PageController(initialPage: this.page);
    //Check connectivity
    new Connectivity().checkConnectivity().then((connectivityResult)=>bme.setIsConnectedToInternet(connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi));
    connectedToInternetSubscription = new Connectivity().onConnectivityChanged.listen((ConnectivityResult connectivityResult) {
      bme.setIsConnectedToInternet(connectivityResult == ConnectivityResult.mobile ||
          connectivityResult == ConnectivityResult.wifi);
      print("Internet Connectivity Changed: "+ bme.getIsConnectedToInternet().toString());
    });
    startSocket();

  }

  @override
  void dispose() {
    super.dispose();
    connectedToInternetSubscription.cancel();
    channel.sink.close();
  }

  void onTap(int index) {
    pageController.animateToPage(
        index, duration: const Duration(milliseconds: 300),
        curve: Curves.ease);
  }

  void onPageChanged(int page) {
    setState(() {
      this.page = page;
    });
  }


}
