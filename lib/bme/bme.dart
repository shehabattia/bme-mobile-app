import 'dart:async';

import 'package:bme/bme/api.dart';
import 'package:bme/models/model_feed.dart';
import 'package:pool/pool.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
Bme bme = new Bme();
class Bme {
//  final BmeApi api = new BmeApi();
  static final Bme _bme = new Bme._internal();
  static bool isConnectedToInternet = true;

  // The controller to receive the input form the app elements
  final socket_feed = StreamController<ModelFeed>();
  Sink<ModelFeed> get query_feed => socket_feed.sink;
  Stream<ModelFeed> get result_feed => socket_feed.stream;

  factory Bme(){
    return _bme;
  }


  bool getIsConnectedToInternet(){
    return isConnectedToInternet;
  }
  void setIsConnectedToInternet(bool is_connected){
    isConnectedToInternet = is_connected;
  }

  int getTimeStamp(){
    return  new DateTime.now().millisecondsSinceEpoch;
  }

  Bme._internal();

  double distanceToKm(double distance) {
    return distance * 100;
  }

  void dispose(){
    socket_feed.close();
  }
}

