//Node server for Bme
//http server:
var http = require('http').createServer(handler).listen(3000);
var fs = require('fs');
const querystring = require('querystring');

//mysql
var mysql = require('mysql')
// Define our db creds
var db = mysql.createConnection({
    host: 'localhost',
    user: 'moon',
    database: 'bme',
    password: '3arenotlonghidden',
	typeCast: function castField( field, useDefaultTypeCasting ) {

        // We only want to cast bit fields that have a single-bit in them. If the field
        // has more than one bit, then we cannot assume it is supposed to be a Boolean.
        if ( ( field.type === "BIT" ) && ( field.length === 1 ) ) {

            var bytes = field.buffer();

            // A Buffer in Node represents a collection of 8-bit unsigned integers.
            // Therefore, our single "bit field" comes back as the bits '0000 0001',
            // which is equivalent to the number 1.
            return( bytes[ 0 ] === 1 );

        }

        return( useDefaultTypeCasting() );

    }
})
// Log any errors connected to the db
db.connect(function(err) {
    if (err) console.log(err)
})

//Web socket:
const WebSocket = require('ws');
const wss = new WebSocket.Server({
    noServer: true
});
var socketCount = 0; //Connected sockets.
//Function to broadcast to all websocket clients.
function broadcast(data) {
    wss.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN) {
            client.send(data);
        }
    });
}
wss.on('connection', function connection(ws) {
    // Socket has connected, increase socket count
    socketCount++
    // Let all sockets know how many are connected
    broadcast('users connected ' + socketCount)
    setInterval(
        () => ws.send(`${new Date()}`),
        1000
    );
    ws.on('message', function incoming(message) {
        console.log('received: %s', message);
        // ws.send('server receieved '+message);

        broadcast('server receieved ' + message)

    });
    ws.on('close', function() {
        // Decrease the socket count on a disconnect, emit
        socketCount--
        broadcast('users connected ' + socketCount)
    });
});
//Handles nginx proxy messing things up for web sockets:
http.on('upgrade', function upgrade(request, socket, head) {
    wss.handleUpgrade(request, socket, head, function done(ws) {
        wss.emit('connection', ws, request);
    });
});
//Routes
// URL ('/') -> GET returns index.html else Error 400
function route_root(req,res){
	fs.readFile(__dirname + '/public/index.html', function(err, data) { //read file index.html in public folder
				if (err) {
					res.writeHead(404, {
						'Content-Type': 'text/html'
					}); //display 404 on error
					return res.end("404 Not Found");
				}
				res.writeHead(200, {
					'Content-Type': 'text/html'
				}); //write HTML
				res.write(data); //write data from index.html
				return res.end();
	
	});
}
function route_get_communities(req,res){
	res.writeHead(200, {
		'Content-Type': 'text/html'
	}); //write HTML
	 db.query('SELECT C.id,G.id FROM communities C JOIN groups G on C.id=G.community_id',function(err,results,fields){
		 res.write('data');
		 
		res.write(JSON.stringify({"status": 200, "error": null, "response": results}));
		console.log(fields);
		 console.log(results);
		 res.end();
	 });
            // .on('result', function(data){
              ////  Push results onto the notes array
               // res.write(data);
            // })
            // .on('end', function(){
               //// Only emit notes after query has been completed
                // return res.end();
            // });
}
function route_get_feed(req,res,url){
	res.writeHead(200, {
		'Content-Type': 'text/html'
	}); //write HTML
	 db.query('SELECT * FROM feed C JOIN groups G on C.id=G.community_id',function(err,results,fields){
		 res.write('data');
		 
		res.write(JSON.stringify({"status": 200, "error": null, "response": results}));
		console.log(fields);
		 console.log(results);
		 res.end();
	 });
            // .on('result', function(data){
              ////  Push results onto the notes array
               // res.write(data);
            // })
            // .on('end', function(){
               //// Only emit notes after query has been completed
                // return res.end();
            // });
}
function route_error(errorType, errorMessage,res){
		res.writeHead(errorType, {
			'Content-Type': 'text/html'
		});
		return res.end(errorType+" "+errorMessage);	
}


//http handler
function handler(req, res) { //create server
	var url = querystring.parse(req.url.substring(2));
	//GET Requests
	if (req.method == 'GET') { 
		if(url['get'] == null){
				return route_root(req,res);
		} 
		else if (url['get']=='communities'){
			return route_get_communities(req,res);
		} 
		else if (url['get']=='feed'){
			return route_get_feed(req,res,url);
		} 
		//Display 404
		else {
			return route_error(404, " Not Found",res);
		}
	}
	// POST Requests
	else if (req.method == 'POST') {
		//        db.query('INSERT INTO notes (note) VALUES (?)', data.note)
		//querystring.parse('w=%D6%D0%CE%C4&foo=bar');
		if (url==='/auth'){
		
		} 
		//Display 400
		else {
			return route_error(400, " Bad Request Error",res);
		}
	}
	//Display 400 if it's not a GET or POST.
	else {
		return route_error(400, " Bad Request Error",res);
	}
}
