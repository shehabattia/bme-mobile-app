-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'communities'
-- 
-- ---

DROP TABLE IF EXISTS `communities`;
		
CREATE TABLE `communities` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `name` VARCHAR(255) NOT NULL,
  `about` MEDIUMTEXT NOT NULL,
  `website` MEDIUMTEXT NOT NULL,
  `is_invite_only` bit NULL DEFAULT 0,
  `timestamp` TIMESTAMP NOT NULL,
  `user_id` INTEGER NULL DEFAULT NULL,
  `role_to_create_groups` INTEGER NULL DEFAULT NULL,
  `role_to_archive_groups` INTEGER NULL DEFAULT NULL,
  `role_to_archive_community` INTEGER NULL DEFAULT NULL,
  `is_archived` bit NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'groups'
-- 
-- ---

DROP TABLE IF EXISTS `groups`;
		
CREATE TABLE `groups` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `community_id` INTEGER NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `is_private` bit NOT NULL DEFAULT 0,
  `dues_amount` DOUBLE NOT NULL,
  `force_show_in_feed` bit NOT NULL DEFAULT 0,
  `default_show_in_feed` bit NOT NULL DEFAULT 1,
  `role_to_post` INTEGER NULL DEFAULT NULL,
  `role_to_reply` INTEGER NULL DEFAULT NULL,
  `is_archived` bit NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'roles'
-- 
-- ---

DROP TABLE IF EXISTS `roles`;
		
CREATE TABLE `roles` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'users'
-- 
-- ---

DROP TABLE IF EXISTS `users`;
		
CREATE TABLE `users` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `email` MEDIUMTEXT NULL DEFAULT NULL,
  `date_joined` TIMESTAMP NULL DEFAULT NULL,
  `last_online` TIMESTAMP NULL DEFAULT NULL,
  `karma` INTEGER NULL DEFAULT 0,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'subscriptions'
-- 
-- ---

DROP TABLE IF EXISTS `subscriptions`;
		
CREATE TABLE `subscriptions` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `group_id` INTEGER NULL DEFAULT NULL,
  `user_id` INTEGER NULL DEFAULT NULL,
  `role` INTEGER NULL DEFAULT NULL,
  `is_paid_dues` bit NULL DEFAULT NULL,
  `show_in_feed` bit NULL DEFAULT 1,
  `show_notifications` bit NULL DEFAULT 1,
  `date_joined` TIMESTAMP NOT NULL,
  `is_active` bit NULL DEFAULT 1,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'feed'
-- 
-- ---

DROP TABLE IF EXISTS `feed`;
		
CREATE TABLE `feed` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `group_id` INTEGER NULL DEFAULT NULL,
  `type` VARCHAR(255) NULL DEFAULT NULL,
  `user_id` INTEGER NULL DEFAULT NULL,
  `title` VARCHAR(255) NULL DEFAULT NULL,
  `body` MEDIUMTEXT NULL DEFAULT NULL,
  `media_id` INTEGER NULL DEFAULT NULL,
  `timestamp` TIMESTAMP NULL DEFAULT NULL,
  `event_date` TIMESTAMP NULL DEFAULT NULL,
  `is_send_notification` bit NULL DEFAULT 1,
  `is_hidden` bit NULL DEFAULT 0,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'replies'
-- 
-- ---

DROP TABLE IF EXISTS `replies`;
		
CREATE TABLE `replies` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `feed_id` INTEGER NULL DEFAULT NULL,
  `message` MEDIUMTEXT NULL DEFAULT NULL,
  `user_id` INTEGER NULL DEFAULT NULL,
  `timestamp` TIMESTAMP NULL DEFAULT NULL,
  `is_hidden` bit NULL DEFAULT 0,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'reactions'
-- 
-- ---

DROP TABLE IF EXISTS `reactions`;
		
CREATE TABLE `reactions` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `feed_id` INTEGER NULL DEFAULT NULL,
  `reply_id` INTEGER NULL DEFAULT NULL,
  `user_id` INTEGER NULL DEFAULT NULL,
  `type` VARCHAR(255) NULL DEFAULT NULL,
  `timestamp` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'media'
-- 
-- ---

DROP TABLE IF EXISTS `media`;
		
CREATE TABLE `media` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `feed_id` INTEGER NULL DEFAULT NULL,
  `media_type` VARCHAR(255) NULL DEFAULT NULL,
  `is_private` bit NULL DEFAULT 0,
  `path` MEDIUMTEXT NULL DEFAULT NULL,
  `timestamp` TIMESTAMP NULL DEFAULT NULL,
  `is_hidden` bit NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Foreign Keys 
-- ---

-- ALTER TABLE `communities` ADD FOREIGN KEY (user_id) REFERENCES `users` (`id`);
-- ALTER TABLE `groups` ADD FOREIGN KEY (community_id) REFERENCES `communities` (`id`);
-- ALTER TABLE `subscriptions` ADD FOREIGN KEY (group_id) REFERENCES `groups` (`id`);
-- ALTER TABLE `subscriptions` ADD FOREIGN KEY (user_id) REFERENCES `users` (`id`);
-- ALTER TABLE `feed` ADD FOREIGN KEY (group_id) REFERENCES `groups` (`id`);
-- ALTER TABLE `feed` ADD FOREIGN KEY (user_id) REFERENCES `users` (`id`);
-- ALTER TABLE `replies` ADD FOREIGN KEY (feed_id) REFERENCES `feed` (`id`);
-- ALTER TABLE `replies` ADD FOREIGN KEY (user_id) REFERENCES `users` (`id`);
-- ALTER TABLE `reactions` ADD FOREIGN KEY (feed_id) REFERENCES `feed` (`id`);
-- ALTER TABLE `reactions` ADD FOREIGN KEY (reply_id) REFERENCES `replies` (`id`);
-- ALTER TABLE `reactions` ADD FOREIGN KEY (user_id) REFERENCES `users` (`id`);
-- ALTER TABLE `media` ADD FOREIGN KEY (feed_id) REFERENCES `feed` (`id`);

-- ---
-- Table Properties
-- ---

-- ALTER TABLE `communities` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `groups` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `roles` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `users` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `subscriptions` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `feed` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `replies` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `reactions` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `media` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

INSERT INTO `communities` (`id`,`name`,`about`,`website`,`is_invite_only`,`timestamp`,`user_id`,`role_to_create_groups`,`role_to_archive_groups`,`role_to_archive_community`,`is_archived`) VALUES
(null,'BME Robotics','Design, build, code robots that save people\'s lives!','http://bmerobotics.com',0,1529558144 ,1,3,3,0,0);
INSERT INTO `groups` (`id`,`community_id`,`name`,`is_private`,`dues_amount`,`force_show_in_feed`,`default_show_in_feed`,`role_to_post`,`role_to_reply`,`is_archived`) VALUES
(null,1,'Team Poof',0,0,0,1,4,4,0);
INSERT INTO `roles` (`id`,`name`) VALUES ('-1','super');
INSERT INTO `roles` (`id`,`name`) VALUES ('0','admin');
INSERT INTO `roles` (`id`,`name`) VALUES ('1','executive');
INSERT INTO `roles` (`id`,`name`) VALUES ('2','leader');
INSERT INTO `roles` (`id`,`name`) VALUES ('3','subleader');
INSERT INTO `roles` (`id`,`name`) VALUES ('4','member');
INSERT INTO `roles` (`id`,`name`) VALUES ('5','guest');
INSERT INTO `users` (`id`,`name`,`email`,`date_joined`,`last_online`,`karma`) VALUES
(null,'Shehab','shehabattia96@gmail.com',1529558144,1529558144,9000);
INSERT INTO `subscriptions` (`id`,`group_id`,`user_id`,`role`,`is_paid_dues`,`show_in_feed`,`show_notifications`,`date_joined`,`is_active`) VALUES
(null,1,1,-1,0,1,1,1529558144,1);
INSERT INTO `feed` (`id`,`group_id`,`type`,`user_id`,`title`,`body`,`media_id`,`timestamp`,`event_date`,`is_send_notification`,`is_hidden`) VALUES
(null,1,'message',1,'Welcome to BME Robotics!','Feel free to leave feedback on improving this app.',null,1529558144,null,1,0);
INSERT INTO `replies` (`id`,`feed_id`,`message`,`user_id`,`timestamp`,`is_hidden`) VALUES
(null,1,'So how was everyone\'s summer?',1,1529558144,0);
INSERT INTO `reactions` (`id`,`feed_id`,`reply_id`,`user_id`,`type`,`timestamp`) VALUES
(null,1,null,1,'like',1529558144);
-- INSERT INTO `media` (`id`,`feed_id`,`media_type`,`is_private`,`path`,`timestamp`,`is_hidden`) VALUES
-- ('','','','','','','');